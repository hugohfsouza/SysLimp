<?php

class ModeloLimpezaController{

	private $modeloLimpeza;
	
	function __construct()
	{
		$this->modeloLimpeza = new ModeloLimpeza();
	}

	public function novoModelo(){

		$novoModeloLimpeza = $this->modeloLimpeza->novoModeloLimpeza();			
		$this->modeloLimpeza = $novoModeloLimpeza;
		include('../View/modeloUnico.php');
									
	}

	public function recuperarModelo($idModelo){

	}

	public function salvarModelo($coordenadasEscolhidas, $descricao, $agentes, $dataHora, $plantaModelo){
		$objetosCoordenada = array();

		foreach ($coordenadasEscolhidas as $posicao) {
			$arrayAux = explode("+", $posicao);
			//$arrayAux[0] = coordenada x
			//$arrayAux[1] = coordenada y

			$objCood = new Coordenada();
			$objCood->setCoordenadaX($arrayAux[0]);
			$objCood->setCoordenadaY($arrayAux[1]);

			$objetosCoordenada[] = $objCood;
		}

		$novoModeloLimpeza = new ModeloLimpeza();

		$objetoAgente = new Agente();			
		$agentesEscolhidos = array();

		foreach ($agentes as $cadaAgente) {
			 $objetoAgente->setIdAgente($cadaAgente);
			 $agentesEscolhidos[] = $objetoAgente;
		}		

		$novoModeloLimpeza->setDescricao($descricao);
		$novoModeloLimpeza->setDataProgramado($dataHora);
		$novoModeloLimpeza->setUsuario($_COOKIE['idUsuario']);
		$novoModeloLimpeza->setTempoEstimado(0);	
		$novoModeloLimpeza->setPlanta($plantaModelo);	
		$novoModeloLimpeza->setAgentes($agentesEscolhidos);
		$novoModeloLimpeza->setCoordenadasParaLimpar($objetosCoordenada);		
		
		$novoModeloLimpeza->salvarModeloLimpeza($novoModeloLimpeza);
	}

	public function listarModelos(){
		$listagemModelos =  ModeloLimpeza::listarModelosLimpeza();
		require("../View/modeloLimpezaPrincipal.php");

	}

	public function alterarModelos($idModelo, $planta, $descricao, $agentes, $dataHora){

	}

}


?>