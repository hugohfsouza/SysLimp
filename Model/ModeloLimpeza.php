<?php
include('conexao.php');

class ModeloLimpeza{

	private $idModelo;
	private $descricao;
	private $tempoEstimado;
	private $dataProgramado;
	private $planta;
	private $usuario;
	private $agentes;
    private $coordenadasParaLimpar;

	function __construct()
	{
	}

	public function novoModeloLimpeza(){
		$novaPlanta = new Planta();
		$novosAgente = new Agente();

		$modeloLimpeza = new ModeloLimpeza();
		$modeloLimpeza->setPlanta($novaPlanta->carregarPlantaUsuario($_COOKIE['idUsuario']));	
		$modeloLimpeza->setAgentes($novosAgente->recuperarDadosAgenteLimpeza(null,$_COOKIE['idUsuario'] ));
		$modeloLimpeza->setDataProgramado('');	
		$modeloLimpeza->setDescricao('');
		$modeloLimpeza->setTempoEstimado('');	
		return $modeloLimpeza;
			
	}

	public function salvarModeloLimpeza($modeloLimpeza){


        $inserirModelo = "INSERT INTO syslimp.modelo
                            (descricao,
                            data,
                            idUsuario,
                            tempoEstimado,
                            idPlanta)
                            VALUES
                            ('{$modeloLimpeza->getDescricao()}',
                            '".str_replace("/", "-", $modeloLimpeza->getDataProgramado())."',
                            {$modeloLimpeza->getUsuario()},
                            0,
                            {$modeloLimpeza->getPlanta()->getIdPlanta()})
                            ";
        mysql_query($inserirModelo);
        $modeloLimpeza->setIdModelo(mysql_insert_id());

        //Salvando as coordenadas escoolhidas pelo usuário
        foreach ($modeloLimpeza->getCoordenadasParaLimpar() as $parOrdenado) {
                $insert = "INSERT INTO 
                            coordenadasmodelo(idModelo,
                                  idUsuario,
                                  idPlanta, 
                                  coordenadaX, 
                                  coordenadaY) 
                            VALUES (
                                  {$modeloLimpeza->getIdModelo()},
                                  {$modeloLimpeza->getUsuario()},
                                  {$modeloLimpeza->getPlanta()->getIdPlanta()},
                                  {$parOrdenado->getCoordenadaX()},
                                  {$parOrdenado->getCoordenadaY()})";    
                mysql_query($insert);
        }


        //Salvando os agentes...

        foreach ($modeloLimpeza->getAgentes() as $agenteUnicos) {
            $insert = "INSERT into 
                                agentesmodelo(
                                        idModelo, 
                                        idAgente) 
                                values (
                                    {$modeloLimpeza->getIdModelo()},
                                    {$agenteUnicos->getIdAgente()}
                                    )";
            mysql_query($insert);
        }         
            
	}

	public function recuperarModeloLimpeza($idModelo){

	}

	public function excluirModeloLimpeza($idModelo){

	}

	public static function listarModelosLimpeza(){
        $selecionaModelosLimpeza = "select idModelo, descricao, tempoEstimado from modelo";
        $blocoSqlSelect = mysql_query($selecionaModelosLimpeza);
        $retorno = array();
        while ($row = mysql_fetch_assoc($blocoSqlSelect)) {
            $retorno[] = $row;
        }
        return $retorno;

	}

    public function getIdModelo(){
        return $this->idModelo;
    }

    public function setIdModelo($idModelo){
        $this->idModelo = $idModelo;
    }


    public function getDescricao(){
        return $this->descricao;
    }


    public function setDescricao($descricao){
        $this->descricao = $descricao;
    }


    public function getTempoEstimado(){
        return $this->tempoEstimado;
    }


    public function setTempoEstimado($tempoEstimado){
        $this->tempoEstimado = $tempoEstimado;
    }


    public function getDataProgramado(){
        return $this->dataProgramado;
    }


    public function setDataProgramado($dataProgramado){
        $this->dataProgramado = $dataProgramado;
    }


    public function getPlanta(){
        return $this->planta;
    }


    public function setPlanta($planta){
        $this->planta = $planta;

    }


    public function getUsuario(){
        return $this->usuario;
    }


    public function setUsuario($usuario){
        $this->usuario = $usuario;

        return $this;
    }


    public function getAgentes(){
        return $this->agentes;
    }


    public function setAgentes($agentes){
        $this->agentes = $agentes;
    }


    public function setCoordenadasParaLimpar($arrayCoordenadas){
            $this->coordenadasParaLimpar = $arrayCoordenadas;
    }

    public function getCoordenadasParaLimpar(){
        return $this->coordenadasParaLimpar;
    }
}
?>